import React, { Component } from 'reactn'
import { Platform, StyleSheet, Text, View, ToastAndroid } from 'react-native'
import firebase from 'react-native-firebase'
import { Button } from 'react-native-paper'
import { GoogleSignin, GoogleSigninButton } from 'react-native-google-signin'

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu'
})

export default class App extends Component {
  onLoginOrRegister = () => {
    GoogleSignin.configure({
      // what API you want to access on behalf of the user, default is email and profile
      webClientId:
        '106095053847-m8b0haslh9koa0sohefg3v3rrb2vi852.apps.googleusercontent.com',
      forceConsentPrompt: true
    })
    GoogleSignin.signOut()
    GoogleSignin.signIn()
      .then(data => {
        alert(1)
        // Create a new Firebase credential with the token
        const credential = firebase.auth.GoogleAuthProvider.credential(
          data.idToken,
          data.accessToken
        )
        // Login with the credential
        return firebase.auth().signInWithCredential(credential)
      })
      .then(user => {
        alert(2)

        // If you need to do anything with the user, do it here
        // The user will be logged in automatically by the
        // `onAuthStateChanged` listener we set up in App.js earlier
      })
      .catch(error => {
        const { code, message } = error

        alert(message + code)
        // For details of error codes, see the docs
        // The message contains the default Firebase string
        // representation of the error
      })
  }
  componentDidMount() {
    firebase
      .database()
      .ref('/sites')
      .push({ test: 'data' })
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to React Native!</Text>
        <Text style={styles.instructions}>To get started, edit App.js</Text>
        <Text style={styles.instructions}>{instructions}</Text>
        <GoogleSigninButton
          value='Iniciar sesion'
          onPress={this.onLoginOrRegister}
          style={{ width: 192, height: 48 }}
          size={GoogleSigninButton.Size.Wide}
          color={GoogleSigninButton.Color.Dark}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5
  }
})
