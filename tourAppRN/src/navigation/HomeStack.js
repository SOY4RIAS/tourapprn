import { createStackNavigator } from 'react-navigation'
import Home from '../screens/Home'
import SiteDetail from '../screens/SiteDetail'
import CrudSite from '../screens/CrudSite'
import ReaderQR from '../screens/ReaderQR'

const HomeStack = createStackNavigator(
	{
		Home: {
			screen: Home,
			navigationOptions: {
				header: null
			}
		},
		SiteDetail: {
			screen: SiteDetail,
			navigationOptions: {
				title: 'Información sitio'
			}
		},
		CrudSite: {
			screen: CrudSite,
			navigationOptions: {
				title: 'Crud Sitio'
			}
		},
		ReaderQR: {
			screen: ReaderQR,
			navigationOptions: {
				header: null
			}
		}
	},
	{
		initialRouteName: 'Home'
	}
)

export default HomeStack
