import React from 'reactn'
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs'
import Icon from 'react-native-vector-icons/FontAwesome'
import { Categories, Chats, Map } from '../screens'
import HomeStack from './HomeStack'
import Configuration from '../screens/Configuration'

const AppStack = createMaterialBottomTabNavigator(
  {
    Home: {
      screen: HomeStack,
      navigationOptions: {
        title: 'Inicio',
        barStyle: { backgroundColor: '#0382d1' },
        tabBarIcon: <Icon color={'white'} size={24} name='home' />
      }
    },
    Categories: {
      screen: Categories,
      navigationOptions: {
        title: 'Destacados',
        barStyle: { backgroundColor: '#6200ee' },
        tabBarIcon: <Icon size={24} name='star' color={'white'} />
      }
    },
    Map: {
      screen: Map,
      navigationOptions: {
        title: 'Mapa',
        barStyle: { backgroundColor: '#6200ee' },
        tabBarIcon: <Icon size={24} name='map-marker' color={'white'} />
      }
    },
    Chats: {
      screen: Chats,
      navigationOptions: {
        title: 'Chats',
        barStyle: { backgroundColor: '#00af52' },
        tabBarIcon: <Icon size={24} name='comments' color={'white'} />
      }
    },
    Configuration: {
      screen: Configuration,
      navigationOptions: {
        title: 'Configuración',
        barStyle: { backgroundColor: '#339AF0' },
        tabBarIcon: <Icon size={24} name='cog' color={'white'} />
      }
    }
  },
  {
    shifting: true,
    activeColor: '#f0edf6',
    initialRouteName: 'Chats'
  }
)

export default AppStack
