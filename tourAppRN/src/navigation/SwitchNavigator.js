import createAnimatedSwitchNavigator from 'react-navigation-animated-switch'
import AppNavigator from './AppNavigator'

const SwitchNavigator = createAnimatedSwitchNavigator(
  {
    AppNavigator,
    App: {
      screen: 'AppNavigator'
    }
  },
  {
    initialRouteName: 'AppNavigator'
  }
)

export default SwitchNavigator
