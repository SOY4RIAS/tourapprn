import React, { Component, useGlobal } from 'reactn'
import {
	IconButton,
	Title,
	Paragraph,
	ActivityIndicator,
	FAB,
	Avatar,
	Subheading,
	TextInput,
	Button,
	Divider
} from 'react-native-paper'
import { StatusBar, Image, View, StyleSheet } from 'react-native'
import Carousel from 'react-native-snap-carousel'
import { SafeAreaView } from 'react-navigation'
import { Dimensions } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import { AirbnbRating } from 'react-native-ratings'
import Icon from 'react-native-vector-icons/FontAwesome'
import Comment from '../../components/Comment'
import firebase from 'react-native-firebase'
import DeviceInfo from 'react-native-device-info'

const EditIcon = ({ navigation }) => {
	return useGlobal()[0].currentUser.user.emailVerified &&
		<IconButton icon='edit' onPress={navigation.getParam('edit')} />
}

class SiteDetail extends Component {
	static navigationOptions = ({ navigation }) => ({
		headerTitle: 'Detalle sitio',
		headerRight: <EditIcon navigation={navigation} />
	})

	constructor(props) {
		super(props)
		this._initStatusBar()
		const { navigation } = props
		let id = navigation.getParam('id', '')
		this.state = {
			loading: false,
			comment: '',
			rating: 0,
			site: this.global.sites.find(item => item.id === id),
			idDevice: DeviceInfo.getUniqueID(),
			name: '',
			id
		}
	}

	load = () => {
		const { comments } = this.state.site
		if (comments) {
			if ((data = comments[this.state.idDevice])) {
				this.setState({ rating: data.stars, comment: data.comment, name: data.name })
			}
		}
		if (this.global.currentUser.user.emailVerified) {
			this.setState({ name: this.global.currentUser.user.displayName })
		}
	}

	_initStatusBar = () => {
		StatusBar.setTranslucent(false)
		StatusBar.setBackgroundColor('white', true)
		StatusBar.setBarStyle('dark-content', true)
	}

	componentDidMount() {
		this.props.navigation.setParams({ edit: this.edit })
		this.load()
	}

	edit = () => {
		const { navigation } = this.props
		const id = navigation.getParam('id', '')
		navigation.navigate('CrudSite', { id })
	}

	_renderItem = ({ item, index }) => {
		if (typeof item === 'string' && item !== null && item !== ''
			&& typeof item !== 'object') {
			if (item.startsWith('http')) {
				return (
					<React.Fragment>
						{(typeof item === 'string' && item !== null && item !== ''
							&& typeof item !== 'object'
						) && <Image
								source={{ uri: item }}
								style={style.image}
								onLoad={() => this.setState({ loading: false })}
								onLoadStart={e => this.setState({ loading: true })}
							/>}
						{this.state.loading && <ActivityIndicator size='large' color='#0000ff' style={style.indicator} />}
					</React.Fragment>
				)
			}
		}
		return <React.Fragment />
	}

	saveComment = () => {
		const site = this.global.sites.find(item => item.id === this.state.id)
		const { idDevice } = this.state
		const data = {
			comment: this.state.comment,
			stars: this.state.rating,
			name: this.state.name ? this.state.name : 'Anonimo'
		}
		firebase.database().ref('/sites').child(`${site.id}/comments/${idDevice}`).update(data)
		alert('Tu calificación ha sido guardada')
	}

	render() {
		let site = this.global.sites.find(item => item.id === this.state.id)
		if (!site) {
			site = this.state.site
		}
		const { comments } = site
		let imagesSite = ['https://www.chargeandparking.com/assets/img/default.jpg']
		if (site !== '') {
			const { images } = site
			if (images) {
				imagesSite = Object.values(images)
			}
		}
		return (
			<SafeAreaView>
				<ScrollView>
					<Carousel
						data={imagesSite}
						renderItem={this._renderItem}
						sliderWidth={sliderWidth}
						itemWidth={itemWidth}
						inactiveSlideOpacity={0.6}
						inactiveSlideScale={0.97}
						contentContainerCustomStyle={{ marginTop: 10 }}
					/>
					<View style={{ margin: 10, marginLeft: 20, marginRight: 20 }}>
						<Title>{site.name}</Title>
						<Paragraph style={{ textAlign: 'justify' }}>{site.description}</Paragraph>
						<Divider />
						<View style={style.score}>
							<Title style={{ textAlignVertical: 'center' }}>Calificacion general</Title>
							<Title
								style={{
									fontSize: 50,
									lineHeight: 50
								}}
							>
								{site.qualification ? site.qualification.toFixed(1) : 0}
								<Icon size={24} name='star' color='#F1C40F' />
							</Title>
						</View>
						<Divider />
						<View style={style.rate}>
							<Title>Califica este lugar</Title>
							{this.state.rating !== 0 && <Button onPress={this.saveComment}>Guardar</Button>}
						</View>
						<AirbnbRating
							count={10}
							reviews={[
								'Uribe',
								'Pesimo',
								'Malo',
								'OK',
								'Bien',
								'Hmm...',
								'Muy bien',
								'Wow',
								'Asombroso',
								'Increible'
							]}
							defaultRating={this.state.rating}
							size={20}
							onFinishRating={rating => this.setState({ rating })}
						/>
						<TextInput
							placeholder='Escribe un comentario'
							value={this.state.comment}
							multiline={true}
							numberOfLines={1}
							onChangeText={comment => this.setState({ comment })}
							style={style.comment}
						/>
						{comments &&
							<React.Fragment>
								<Title style={style.textComment}>Comentarios</Title>
								{Object.keys(comments).map((comment, index) => (
									<React.Fragment key={index}>
										{comments[comment] && (
											<React.Fragment>
												<Comment data={comments[comment]} />
												<Divider />
											</React.Fragment>
										)}
									</React.Fragment>
								))}
							</React.Fragment>
						}
					</View>
				</ScrollView>
			</SafeAreaView>
		)
	}
}

const screenWidth = Math.round(Dimensions.get('window').width)
function wp(percentage) {
	const value = percentage * screenWidth / 100
	return Math.round(value)
}
const slideWidth = wp(90)
const sliderWidth = screenWidth
const itemWidth = slideWidth

const style = StyleSheet.create({
	image: {
		width: '100%',
		height: screenWidth * 0.7,
		borderRadius: 10
	},
	score: {
		flexDirection: 'row',
		justifyContent: 'space-evenly',
		marginTop: 30,
		marginBottom: 30
	},
	rate: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginTop: 10
	},
	indicator: {
		position: 'absolute',
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		justifyContent: 'center',
		alignItems: 'center'
	},
	textComment: {
		marginTop: 40
	},
	comment: {
		backgroundColor: 'white',
		marginTop: 10
	}
})

export default SiteDetail
