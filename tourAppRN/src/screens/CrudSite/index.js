import React, { Component } from 'reactn';
import {
	StyleSheet, View, FlatList, Image, Dimensions, StatusBar
	, Alert
} from 'react-native';
import { TextInput, Button, Title, IconButton } from 'react-native-paper';
import { ScrollView } from 'react-native-gesture-handler';
import SelectCoordinates from '../../components/Dialogs/SelectCoordinates';
import ImagePicker from 'react-native-image-picker';
import firebase from 'react-native-firebase';
import { SafeAreaView, NavigationEvents } from 'react-navigation';

class CrudSite extends Component {
	static navigationOptions = ({ navigation }) => ({
		headerTitle: 'Crud sitio',
		headerRight: <IconButton icon='save' onPress={navigation.getParam('save')} />
	});

	initialState = {
		form: {
			name: '',
			description: '',
			address: '',
			email: '',
			webpage: '',
			coordinates: {
				latitude: null,
				longitude: null
			},
			images: {}
		},
		images: []
	};

	constructor(props) {
		super(props);
		this.state = this.initialState;
		this._initStatusBar();
	}

	_initStatusBar = () => {
		StatusBar.setBackgroundColor('white', true);
		StatusBar.setTranslucent(false);
		StatusBar.setBarStyle('dark-content', true);
	};

	getValues = () => {
		const id = this.props.navigation.getParam('id');
		if (id) {
			let site = this.global.sites.find(item => item.id === id);
			site.images = site.images || {}
			this.setState({ form: site });
			this.dialog.updateCoordinate(site.coordinates);
		}
	};

	componentDidMount() {
		this.getValues();
		this.props.navigation.setParams({ save: this.save });
	}

	save = async () => {
		let { form } = this.state;
		let id = 0
		if (form.id) {
			id = form.id
		} else {
			id = firebase.database().ref('/sites').push({ name: form.name }).key;
		}
		await this.uploadImages(id);
		await firebase.database().ref(`/sites`).child(id).update(form);
		this.setState({ ...this.initialState });

		alert('Sitio guardado');

		firebase
			.database()
			.ref('/devices_token')
			.once('value')
			.then(snapshot => {
				snapshot.forEach(token => {
					console.log(token);
				});
			})
			.catch(e => console.log(e));
	};

	showImagePiker = () => {
		// More info on all the options is below in the API Reference... just some common use cases shown here
		const options = {
			title: 'Añadir imagen',
			takePhotoButtonTitle: 'Tomar foto',
			chooseFromLibraryButtonTitle: 'Seleccionar de la galeria',
			cancelButtonTitle: 'Cancelar',
			storageOptions: {
				skipBackup: true,
				path: 'images'
			}
		};
		ImagePicker.showImagePicker(options, response => {
			if (response.didCancel) {
				console.log('User cancelled image picker');
			}
			else if (response.error) {
				console.log('ImagePicker Error: ', response.error);
			}
			else if (response.customButton) {
				console.log('User tapped custom button: ', response.customButton);
			}
			else {
				const source = { uri: response.uri, path: response.path };
				let { form } = this.state;
				form.images = form.images || {}
				form.images[this.randomName()] = source
				this.setState({ form });
			}
		});
	};

	randomName = () => {
		return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
	};

	uploadImages = uid => {
		const { images } = this.state.form
		const data = Object.entries(images).map(([key, value]) => ({ key, value }))
		data.forEach(img => {
			if (typeof img.value === 'string') {
				return
			}
			const ext = img.value.path.split('.').pop(); // Extract image extension
			const filename = this.randomName() + `.${ext}`; // Generate unique name
			firebase.storage().ref(`sites/${uid}/${filename}`).putFile(img.value.path)
				.then(snapshot => {
					if (snapshot.state === firebase.storage.TaskState.SUCCESS) {
						images[img.key] = snapshot.downloadURL
						this.setState(prev => ({ form: { ...prev.form, images } }))
						firebase.database().ref(`/sites`).child(uid).update({ images });
					}
				})
		});
	};

	existsURL = (object, url) => {
		Object.values(object).forEach(value => {
			if (value === url) {
				return true;
			}
		});
		return false;
	};

	renderImage = ({ item, index }) => {
		return <View style={{ width: itemWidth, height: itemWidth, margin: 2 }}>
			<Image style={{ flex: 1 }} source={item} />
			<IconButton icon='delete'
				color='red'
				size={15}
				style={styles.deleteImage}
				onPress={() =>
					Alert.alert(
						'Alerta',
						'¿Seguro que deseas borrar la image?',
						[
							{
								text: 'Cancelar',
							},
							{
								text: 'Confirmar', onPress: () => {
									const key = Object.keys(this.state.form.images)[index]
									const { form } = this.state
									delete form.images[key]
									this.setState({ form })
								}
							},
						],
						{ cancelable: false },
					)
				}
			/>
		</View>
	}

	handleChange = key => value => {
		this.setState(prev => ({
			form: { ...prev.form, [key]: value }
		}));
	};

	getImages = (images) => {
		if (images) {
			return Object.values(images).map((image) => {
				let source = {}
				if (typeof image === 'string') {
					source = { uri: image }
				} else {
					source = image
				}
				return source
			})
		}
		return []
	}

	render() {
		const { name, description, address, email,
			webpage, coordinates, images } = this.state.form
		return (
			<SafeAreaView style={styles.container}>
				<ScrollView>
					<TextInput label='Nombre' mode='outlined' value={name} onChangeText={this.handleChange('name')} />
					<TextInput
						label='Descripción'
						mode='outlined'
						multiline={true}
						numberOfLines={6}
						value={description}
						onChangeText={this.handleChange('description')}
					/>
					<TextInput label='Email' mode='outlined' value={email} onChangeText={this.handleChange('email')} />
					<TextInput
						label='Pagina'
						mode='outlined'
						value={webpage}
						onChangeText={this.handleChange('webpage')}
					/>
					<Button
						style={{ marginTop: 10, marginBottom: 15 }}
						icon='room'
						mode='contained'
						onPress={() => this.setGlobal({ dialogCoor: true })}
					>
						Seleccionar ubicación
					</Button>
					<View
						style={{
							flex: 1,
							flexDirection: 'row',
							justifyContent: 'space-between'
						}}
					>
						<Title>Imagenes</Title>
						<Button icon='camera' onPress={this.showImagePiker}>
							Añadir
						</Button>
					</View>
					<FlatList
						data={this.getImages(images)}
						extraData={this.state}
						keyExtractor={(item, key) => key.toString()}
						renderItem={this.renderImage}
						numColumns={4}
						horizontal={false}
					/>
				</ScrollView>
				<SelectCoordinates
					ref={ref => (this.dialog = ref)}
					updateCoor={this.handleChange('coordinates')}
					coordinate={coordinates}
				/>
			</SafeAreaView>
		);
	}
}
const { width } = Dimensions.get('window');
const itemWidth = width / 4 - 10;
const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: 10
	},
	deleteImage: {
		position: 'absolute',
		top: -14,
		right: -14,
	}
});

export default CrudSite;
