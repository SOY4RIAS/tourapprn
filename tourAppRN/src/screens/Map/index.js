import React, { Component } from 'reactn';
import { SafeAreaView } from 'react-navigation';
import MapView, { Marker, Circle } from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';
import { PermissionsAndroid, View, Text } from 'react-native';
import { FAB, IconButton } from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome';
import MapViewDirections from 'react-native-maps-directions';
import { DIRECTIONS_KEY } from '../../tools/constants';
import { NavigationEvents } from 'react-navigation'
import { Button, Paragraph, Dialog, Title } from 'react-native-paper'
import Slider from '@react-native-community/slider'

class Map extends Component {
	constructor() {
		super();
		this.state = {
			permission: false,
			userLocation: {
				latitude: null,
				longitude: null
			},
			showRoute: false,
			origin: {},
			destination: {},
			metrics: {
				distance: '',
				duration: ''
			},
			status: false,
			visibleDialog: false,
			metros: 100,
			filter: false
		};
	}



	getLocation = async () => {
		// Instead of navigator.geolocation, just use Geolocation.
		await this.requestLocationPermission();
		if (this.state.permission) {
			await Geolocation.getCurrentPosition(
				position => {
					let { latitude, longitude } = position.coords;
					this.setState({
						userLocation: {
							latitude,
							longitude
						}
					});
				},
				error => {
					console.log(error.code, error.message);
				},
				{ enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
			);
			return true;
		}
		else {
			return false;
		}
	};

	requestLocationPermission = async () => {
		try {
			const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, {
				title: 'Example App',
				message: 'Example App access to your location '
			});
			if (granted === PermissionsAndroid.RESULTS.GRANTED) {
				this.setState({ permission: true });
			}
			else {
				this.setState({ permission: false });
			}
		} catch (err) {
			console.warn(err);
		}
	};

	_setRoute = async coordinates => {
		if (!await this.getLocation()) return;

		this.setState(prev => {
			return { destination: coordinates, showRoute: true };
		});
	};

	_openRouteDetails = ({ distance, duration, coordinates }) => {
		this.mapView.fitToCoordinates(coordinates, {
			edgePadding: {
				right: 100,
				bottom: 100,
				left: 100,
				top: 100
			}
		});

		this.setState({
			metrics: { distance: Math.round(distance), duration: Math.round(duration) }
		});
	};
	mapView = null;

	changeStatus = () => {
		this.setState(prev => ({ status: !prev.status }))
	}

	_showDialog = () => this.setState({ visibleDialog: true })

	_hideDialog = () => this.setState({ visibleDialog: false })

	filter = () => {
		this.getLocation()
		this.setState({ filter: true, visibleDialog: false })
	}

	render() {
		let { sites } = this.global;
		const { filter, userLocation, metros } = this.state
		let { latitude, longitude } = userLocation;
		return (
			<SafeAreaView style={{ flex: 1 }}>
				<NavigationEvents onDidBlur={this.changeStatus} onDidFocus={this.changeStatus} />
				{this.state.status &&
					<MapView
						style={{ flex: 1 }}
						initialRegion={{
							latitude: 3.430694,
							longitude: -76.542292,
							latitudeDelta: 0.0922,
							longitudeDelta: 0.0421
						}}
						cacheEnabled
						followsUserLocation
						showsUserLocation
						loadingEnabled
						userLocationAnnotationTitle='Mi ubicacion'
						ref={c => (this.mapView = c)}
					>
						{(filter && latitude !== null) &&
							<Circle
								center={this.state.userLocation}
								radius={this.state.metros}
								fillColor={'#4285f438'}
								strokeColor={'#4285F4'}
							/>
						}
						{sites.map((site, index) => {
							if (site.coordinates !== undefined) {
								if (filter) {
									if (getDistance(userLocation, site.coordinates) < metros) {
										return (
											<Marker
												onPress={() => this._setRoute(site.coordinates)}
												key={index}
												coordinate={site.coordinates}
												title={site.name}
											/>
										);
									}
								} else {
									return (
										<Marker
											onPress={() => this._setRoute(site.coordinates)}
											key={index}
											coordinate={site.coordinates}
											title={site.name}
										/>
									);
								}
							}
						})}

						{this.state.showRoute && (
							<MapViewDirections
								optimizeWaypoints
								mode='DRIVING'
								onReady={this._openRouteDetails}
								geodesic
								strokeWidth={3}
								strokeColor={'#8b2246'}
								origin={this.state.userLocation}
								destination={this.state.destination}
								apikey={DIRECTIONS_KEY}
							/>
						)}
					</MapView>
				}
				{this.state.showRoute && (
					<View
						style={{
							position: 'absolute',
							margin: 20,
							left: 0,
							bottom: 20,
							backgroundColor: 'white',
							width: 200,
							height: 100,
							borderRadius: 20,
							shadowColor: '#000',
							shadowOffset: { width: 1, height: 62 },
							shadowOpacity: 0.8,
							shadowRadius: 2,
							elevation: 1,
							justifyContent: 'center',
							alignItems: 'center'
						}}
					>
						<Text>Distancia: {this.state.metrics.distance} KM</Text>
						<Text>Duracion: {this.state.metrics.duration} Min</Text>
						<IconButton icon='clear' onPress={() => this.setState({ showRoute: false })} />
					</View>
				)}

				<FAB
					style={{
						position: 'absolute',
						margin: 16,
						right: 0,
						bottom: 0
					}}
					icon={filter ? 'cancel' : 'location-on'}
					onPress={!filter ? this._showDialog : () => { this.setState({ filter: false }) }}
				/>
				<Dialog
					visible={this.state.visibleDialog}
					onDismiss={this._hideDialog}
					dismissable={false}>
					<Dialog.Title>Metros a la redonda</Dialog.Title>
					<Dialog.Content>
						<Title style={{ alignSelf: 'center' }}>{this.state.metros}m</Title>
						<Slider
							style={{ width: 200, height: 40, alignSelf: 'center' }}
							minimumValue={100}
							maximumValue={1000}
							value={this.state.metros}
							minimumTrackTintColor="#03DAC4"
							maximumTrackTintColor="#03DAC4"
							onValueChange={(val) => this.setState({ metros: parseInt(val) })}
						/>
					</Dialog.Content>
					<Dialog.Actions>
						<Button onPress={this._hideDialog}>Cancelar</Button>
						<Button onPress={this.filter}>Filtrar</Button>
					</Dialog.Actions>
				</Dialog>
			</SafeAreaView>
		);
	}
}

function getDistance(p1, p2) {
	rad = x => x * Math.PI / 180;
	var R = 6378137; //radio de la tierra en metros
	var dLat = rad(p2.latitude - p1.latitude);
	var dLong = rad(p2.longitude - p1.longitude);
	var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(rad(p1.latitude)) * Math.cos(rad(p2.latitude)) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	var d = R * c;
	return d;
}

export default Map;
