import React, { Component } from 'reactn'
import { View, FlatList, StatusBar, StyleSheet, Image } from 'react-native'
import { NavigationEvents } from 'react-navigation'
import { Title, Paragraph } from 'react-native-paper'
import { TabView, TabBar } from 'react-native-tab-view'
// import Destacados from './Destacados'

class Categories extends Component {
	constructor(props) {
		super(props)
		this.state = {
			sites: this.global.sites,
			navState: {
				index: 0,
				routes: [
					{ key: 'Ranking', title: 'Destacados', icon: 'map-marker' },
					{ key: 'Favorites', title: 'Favoritos', icon: 'bag-personal' }
				]
			}
		}
	}

	_changeStatusBar = () => {
		StatusBar.setTranslucent(false)
		StatusBar.setBackgroundColor('white', true)
		StatusBar.setBarStyle('dark-content', true)
	}

	componentDidMount() {
		const datos = this.state.sites.sort(
			(a, b) => (a.qualification > b.qualification ? a.qualification === undefined && -1 : 1)
		)
		this.setState({ sites: datos })
	}

	handleIndexChange = index => {
		this.setState(prev => ({ navState: { ...prev.navState, index } }))
	}

	_sortItems = unRelatedItems => {
		const datos = unRelatedItems.sort(
			(a, b) => (a.qualification > b.qualification ? a.qualification === undefined && -1 : 1)
		)
		return datos
	}

	_renderItem = ({ item, index }) => {
		let imageSite = 'https://www.chargeandparking.com/assets/img/default.jpg'
		if (item.images) {
			imageSite = Object.values(item.images)[0]
		}
		return (
			<React.Fragment>
				<View style={style.siteRanking}>
					<Image
						source={{ uri: imageSite }}
						style={style.image}
						onLoad={() => this.setState({ loading: false })}
						onLoadStart={e => this.setState({ loading: true })}
					/>
					<View style={{ margin: 4, padding: 20, width: '80%' }}>
						<Title numberOfLines={1}>{`${index + 1}. ${item.name}`}</Title>
						<Paragraph numberOfLines={3}>{item.description}</Paragraph>
					</View>
				</View>
			</React.Fragment>
		)
	}

	render() {
		let unRelatedItems = [ ...this.global.sites ]

		const items = this._sortItems(unRelatedItems)

		const renderScene = ({ route }) => {
			console.log(route)
			switch (route.key) {
				case 'Ranking':
					return (
						<FlatList
							style={{ flex: 1 }}
							data={items}
							renderItem={this._renderItem}
							keyExtractor={(item, index) => index.toString()}
						/>
					)
				case 'Favorites':
					return <React.Fragment />
			}
		}
		return (
			<React.Fragment>
				<NavigationEvents onDidFocus={this._changeStatusBar} />
				<TabView
					style={{ flex: 1 }}
					navigationState={this.state.navState}
					renderScene={renderScene}
					// renderTabBar={this.renderTabBar}
					onIndexChange={this.handleIndexChange}
				/>
			</React.Fragment>
		)
	}
}

const style = StyleSheet.create({
	image: {
		width: 100,
		height: 100,
		borderTopLeftRadius: 8,
		borderBottomLeftRadius: 8
	},
	siteRanking: {
		margin: 8,
		flexDirection: 'row',
		elevation: 5,
		backgroundColor: 'white',
		borderWidth: 0.2,
		borderRadius: 8,
		position: 'relative',
		paddingRight: 50
	}
})

export default Categories
