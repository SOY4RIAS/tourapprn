import React, { Component, Fragment } from 'reactn';
import { Block } from 'galio-framework';
import { Title } from 'react-native-paper';
import { StyleSheet, StatusBar, Image, TouchableOpacity, Text } from 'react-native';
import { SafeAreaView, NavigationEvents } from 'react-navigation';
import firebase from 'react-native-firebase';
import { GoogleSigninButton, GoogleSignin } from 'react-native-google-signin';
import { setItem } from '../../tools/storage';

class Configuration extends Component {
	constructor() {
		super();
		this.setupGoogleSignin();
	}

	setupGoogleSignin = () => {
		GoogleSignin.configure({
			// what API you want to access on behalf of the user, default is email and profile
			webClientId: '106095053847-m8b0haslh9koa0sohefg3v3rrb2vi852.apps.googleusercontent.com',
			forceConsentPrompt: true
		});
	};

	onLoginOrRegister = () => {
		GoogleSignin.signIn()
			.then(data => {
				const credential = firebase.auth.GoogleAuthProvider.credential(data.idToken, data.accessToken);
				return firebase.auth().signInWithCredential(credential);
			})
			.then(async credential => {
				await setItem('currentUser', credential);
				this.setGlobal({ currentUser: credential });
			})
			.catch(error => {
				const { code, message } = error;
				alert(message);
			});
	};

	logoutGoogle = async () => {
		try {
			await GoogleSignin.revokeAccess();
			await GoogleSignin.signOut();
			firebase.auth().signInAnonymously().then(async credential => {
				await setItem('currentUser', credential);
				this.setGlobal({ currentUser: credential });
			});
		} catch (error) {
			console.error(error);
		}
	};

	_changeStatusBar = () => {
		StatusBar.setBackgroundColor(styles.screenContainer.backgroundColor, true);
		StatusBar.setBarStyle('light-content', true);
	};
	render() {
		const { currentUser } = this.global;
		console.log(currentUser);

		if (currentUser['user']) {
			currentUser.additionalUserInfo.isNewUser = !currentUser.user.emailVerified;
		}

		return (
			Object.keys(currentUser).length !== 0 && (
				<Fragment>
					<NavigationEvents onDidFocus={this._changeStatusBar} />
					<SafeAreaView style={styles.screenContainer}>
						<Block style={styles.profileContainer}>
							<Image
								source={{
									uri: currentUser.additionalUserInfo.isNewUser
										? 'https://cdn3.iconfinder.com/data/icons/avatars-add-on-pack-2/48/v-33-512.png'
										: currentUser.user.photoURL
								}}
								style={styles.profileImage}
							/>
						</Block>
						<Block style={styles.contentContainer}>
							{currentUser.additionalUserInfo.isNewUser ? (
								<GoogleSigninButton
									value='Conectar'
									onPress={this.onLoginOrRegister}
									style={{ width: 200, height: 48 }}
									size={GoogleSigninButton.Size.Wide}
									color={GoogleSigninButton.Color.Light}
								/>
							) : (
								<Title>¡Bienvenido!</Title>
							)}
							{!currentUser.additionalUserInfo.isNewUser && (
								<TouchableOpacity onPress={this.logoutGoogle}>
									<Text>Cerrar Sesion en Google</Text>
								</TouchableOpacity>
							)}
						</Block>
					</SafeAreaView>
				</Fragment>
			)
		);
	}
}

const styles = StyleSheet.create({
	screenContainer: {
		flex: 1,
		backgroundColor: '#339AF0'
	},
	profileContainer: {
		flex: 1
	},
	contentContainer: {
		flex: 1,
		alignItems: 'center'
	},
	profileImage: {
		shadowOffset: { width: 0, height: 2 },
		shadowColor: 'rgba(0,0,0,0.03)',
		shadowOpacity: 1,
		shadowRadius: 4,
		backgroundColor: 'rgba(255,255,255,0.5)',
		alignSelf: 'center',
		marginTop: '20%',
		marginBottom: '5%',
		height: 150,
		width: 150,
		borderRadius: 150
	}
});

export default Configuration;
