import React, { Component } from 'reactn'
import { Text, SafeAreaView } from 'react-native'
import { GiftedChat } from 'react-native-gifted-chat'
import firebase from 'react-native-firebase'
import DeviceInfo from 'react-native-device-info'

class Chats extends Component {
  constructor() {
    super()
    this.state = {
      messages: []
    }
    this.load()
  }

  load = async () => {
    try {
      const starCountRef = firebase.database().ref('chat/messages')
      starCountRef.on('value', snapshot => {
        this.setState({ messages: [] })
        snapshot.forEach(data => {
          if (data.val()) {
            const message = data.val()
            const messages = [message, ...this.state.messages]
            this.setState({ messages })
          }
        })
      })
    } catch {
      alert('Error')
    }
  }

  sendMessage = (messages = []) => {

    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, messages),
    }), () => {
      let { messages } = this.state
      firebase
        .database()
        .ref('chat')
        .child('messages')
        .push(messages[0])
    })
  }

  render() {
    return (
      <GiftedChat
        isAnimated
        placeholder='Escribe un Mensaje'
        messages={this.state.messages}
        onSend={this.sendMessage}
        user={{
          _id: DeviceInfo.getUniqueID()
        }}
      />
    )
  }
}

export default Chats
