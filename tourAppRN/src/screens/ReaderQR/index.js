import React, { Component, Fragment } from 'reactn'
import { View, ActivityIndicator, StatusBar, Alert } from 'react-native'
import { CameraKitCamera, CameraKitCameraScreen } from 'react-native-camera-kit'
import ScanBarCode from 'react-native-scan-barcode'
import { withNavigation } from 'react-navigation'

class ReaderQR extends Component {
	camera = null

	state = {
		canOpen: false,
		scanValid: true,
	}
	constructor(props) {
		super(props)
		this._validate()
		StatusBar.setTranslucent(false)
		StatusBar.setBackgroundColor('black', true)
		StatusBar.setBarStyle('light-content', true)
	}

	_validate = async () => {
		const isCameraAuthorized = await CameraKitCamera.checkDeviceCameraAuthorizationStatus()

		if (isCameraAuthorized == -1 || !isCameraAuthorized) {
			const isUserAuthorizedCamera = await CameraKitCamera.requestDeviceCameraAuthorization()
			this.setState({ canOpen: isUserAuthorizedCamera })
		}
		else if (isCameraAuthorized) {
			this.setState({ canOpen: true })
		}
	}

	onBottomButtonPressed = e => {
		console.warn(e)
	}

	barcodeReceived = (e) => {
		if (this.global.sites.find(item => item.id === e.data)) {
			this.props.navigation.navigate('SiteDetail', { id: e.data })
		} else {
			if (this.state.scanValid) {
				this.setState({ scanValid: false })
				Alert.alert(
					'Alerta',
					'El codigo scaneado no es valido',
					[
						{ text: 'OK', onPress: () => this.setState({ scanValid: true }) },
					],
					{ cancelable: false },
				);
			}
		}
	}

	render() {
		return (
			<Fragment>
				{this.state.canOpen ? (
					<ScanBarCode
						onBarCodeRead={this.barcodeReceived}
						style={{ flex: 1 }}
						torchMode={this.state.torchMode}
						cameraType={this.state.cameraType}
					/>
				) : (
						<ActivityIndicator size='large' color='red' />
					)}
			</Fragment>
		)
	}
}

export default withNavigation(ReaderQR)
