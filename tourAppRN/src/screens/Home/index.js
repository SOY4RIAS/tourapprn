import React, { Component, setGlobal, getGlobal } from 'reactn'
import { SafeAreaView, StyleSheet, View, Text, StatusBar, ScrollView } from 'react-native'
import { FAB } from 'react-native-paper'
import CardSite from '../../components/CardSite'
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler'
import ModalSite from '../../components/ModalSite'
import firebase from 'react-native-firebase'
import { setItem, getItem } from '../../tools/storage'
import Header from '../../components/Header'
import { NavigationEvents } from 'react-navigation'

setGlobal({
	modalVisible: false,
	sites: []
})

class Home extends Component {
	constructor() {
		super()
		this._loadData()
	}

	_initStatusBar = () => {
		StatusBar.setTranslucent(true)
		StatusBar.setBackgroundColor('rgba(0,0,0, 0.2)', true)
		StatusBar.setBarStyle('light-content', true)
	}

	_renderItem = ({ item }) => (
		<TouchableOpacity onPress={() => this.props.navigation.navigate('SiteDetail', { id: item.id })}>
			<CardSite data={item} />
		</TouchableOpacity>
	)

	_loadData = () => {
		this._loadOffline()
			.then(() => {
				this.load().catch(err => {
					console.error(err)
				})
			})
			.catch(err => {
				console.error(err)
			})
	}

	_loadOffline = async () => {
		let sites = await getItem('sites')
		this.setGlobal({ ...sites })
	}

	load = async () => {
		try {
			const starCountRef = firebase.database().ref('sites/')
			starCountRef.on('value', snapshot => {
				this.setGlobal({ sites: [], backupSites: [] })
				snapshot.forEach(data => {
					if (data.val()['name']) {
						let sitio = data.val()
						sitio.id = data.key
						this.setGlobal(prev => {
							nextState = {
								sites: [ ...prev.sites, sitio ],
								backupSites: [ ...prev.backupSites, sitio ]
							}
							setItem('sites', nextState)
							return nextState
						})
					}
				})
			})
		} catch (e) {
			alert('Error')
		}
	}

	render() {
		let { sites, currentUser } = this.global
		return (
			<React.Fragment>
				<NavigationEvents onDidFocus={this._initStatusBar} />
				<SafeAreaView style={{ flex: 1 }}>
					<ScrollView>
						<Header />
						<FlatList
							style={{ flex: 1 }}
							data={sites}
							renderItem={this._renderItem}
							keyExtractor={(item, index) => index.toString()}
						/>
					</ScrollView>
				</SafeAreaView>

				{Object.keys(currentUser).length !== 0 &&
				!currentUser.additionalUserInfo.isNewUser && (
					<FAB
						style={styles.fab}
						icon='add'
						color='#8b2246'
						onPress={() => this.props.navigation.navigate('CrudSite')}
					/>
				)}

				{Object.keys(currentUser).length !== 0 && !currentUser.additionalUserInfo.isNewUser ? (
					<FAB
						style={styles.fabCamera}
						small
						icon='camera'
						color='white'
						onPress={() => this.props.navigation.navigate('ReaderQR')}
					/>
				) : (
					<FAB
						style={styles.fabCameraAlt}
						small
						icon='camera'
						color='white'
						onPress={() => this.props.navigation.navigate('ReaderQR')}
					/>
				)}
				{!this.global.online && (
					<View style={styles.networkIndicator}>
						<Text style={{ color: 'white' }}>{this.global.online ? 'EN LINEA' : 'SIN INTERNET'}</Text>
					</View>
				)}
			</React.Fragment>
		)
	}
}

const styles = StyleSheet.create({
	fab: {
		position: 'absolute',
		margin: 16,
		right: 0,
		bottom: 0,
		backgroundColor: '#ff4081'
	},
	fabCamera: {
		position: 'absolute',
		margin: 16,
		right: 10,
		bottom: 60,
		backgroundColor: '#ff4081'
	},
	fabCameraAlt: {
		position: 'absolute',
		margin: 16,
		right: 15,
		bottom: 0,
		backgroundColor: '#ff4081'
	},
	networkIndicator: {
		justifyContent: 'center',
		backgroundColor: 'gray',
		width: '100%',
		position: 'absolute',
		height: 20,
		bottom: 0
	}
})

export default Home
