import React, { Component } from 'reactn'
import { StyleSheet } from 'react-native'
import { View } from 'react-native'
import { Button, Paragraph, Dialog, Portal } from 'react-native-paper'
import MapView, { Marker } from 'react-native-maps'

class SelectCoordinates extends Component {

  constructor(props) {
    super(props)
    const { coordinate } = this.props
    if (coordinate.latitude === null) {
      coordinate.latitude = 3.441300
      coordinate.longitude = -76.524022
    }
    this.state = {
      coordinate
    }
  }

  updateCoordinate = (coordinate) => {
    this.setState({ coordinate })
  }

  _hideDialog = () => this.setGlobal({ dialogCoor: false });

  render() {
    return (
      <Dialog
        visible={this.global.dialogCoor}
        dismissable={false}
        onDismiss={this._hideDialog}>
        <Dialog.Title>Seleccionar la ubicación</Dialog.Title>
        <Dialog.Content style={{ paddingBottom: 0 }}>
          <View style={{ height: 400 }}>
            <MapView
              style={{ width: '100%', flex: 1 }}
              initialRegion={{
                ...this.state.coordinate,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
              }}
              draggable
            >
              <Marker
                draggable
                coordinate={this.state.coordinate}
                onDragEnd={({ nativeEvent }) =>
                  this.setState({ coordinate: nativeEvent.coordinate })}
              />
            </MapView>
          </View>
        </Dialog.Content>
        <Dialog.Actions>
          <Button onPress={() => {
            this._hideDialog()
            this.setState({ coordinate: this.props.coordinate })
          }}>
            Cancelar
          </Button>
          <Button onPress={() => {
            this.props.updateCoor(this.state.coordinate)
            this._hideDialog()
          }
          }>
            Seleccionar
          </Button>
        </Dialog.Actions>
      </Dialog>
    )
  }
}

export default SelectCoordinates