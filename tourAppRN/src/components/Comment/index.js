import React, { Component } from 'react'
import { View, StyleSheet } from 'react-native'
import { Avatar, Subheading, Paragraph } from 'react-native-paper'
import { Rating } from 'react-native-ratings'

class Comment extends Component {
  render() {
    const { avatar, comment, name = 'Anonimo', stars } = this.props.data
    return (
      <View style={style.container}>
        <View style={{ flexDirection: 'row' }}>{
          avatar ?
            <Avatar.Image size={30} source={{ uri: avatar }} />
            :
            <Avatar.Text size={40} label={name.substring(0, 1)} />
        }
          <Subheading style={style.name}>{name}</Subheading>
        </View>
        <Rating
          ratingCount={10}
          imageSize={10}
          startingValue={stars}
          readonly
          style={style.rating}
        />
        <Paragraph>{comment}</Paragraph>
      </View>
    )
  }
}

const style = StyleSheet.create({
  container: {
    marginTop: 15,
    marginBottom: 10
  },
  name: {
    marginLeft: 15,
  },
  rating: {
    alignSelf: 'flex-start',
    marginTop: 10,
    marginBottom: 5
  }
})

export default Comment