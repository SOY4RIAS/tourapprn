import React, { useState, useGlobal, useDispatch } from 'reactn'
import { View, StyleSheet, Text } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { TextInput, IconButton } from 'react-native-paper'

const Header = ({}) => {
	const [ search, setSearch ] = useState('')
	const [ canSearch, openSearch ] = useState(false)
	const searchSites = useDispatch('SEARCH_SITES')

	return (
		<View>
			<View style={styles.container}>
				<LinearGradient
					start={{ x: 0, y: 0 }}
					end={{ x: 1, y: 0 }}
					colors={[ 'rgb(225,225,225)', '#0288D1' ]}
					style={styles.shape}
				/>
				<LinearGradient
					start={{ x: 1, y: 0 }}
					end={{ x: 0, y: 1 }}
					colors={[ '#FF4081', 'rgb(225,225,225)' ]}
					style={styles.shape2}
				/>
				<View style={styles.banner}>
					<Text style={styles.title}>TourApp</Text>
					<View style={styles.searchBox}>
						<TextInput
							value={search}
							style={styles.search}
							label='Buscar'
							placeholderTextColor='#0288D1'
							theme={{
								dark: true,
								colors: {
									primary: '#0288D1',
									accent: 'white',
									text: 'black'
								}
							}}
							onChangeText={text => {
								setSearch(text)
								searchSites(text)
							}}
						/>

						<IconButton
							color='white'
							icon='search'
							onPress={() => {
								searchSites(search)
								openSearch(true)
							}}
							style={styles.searchButton}
						/>
						{search != '' && (
							<IconButton
								color='white'
								icon='clear'
								onPress={() => {
									setSearch('')
									searchSites('')
								}}
								style={styles.searchClear}
							/>
						)}
					</View>
				</View>
			</View>
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		height: 200
	},
	shape: {
		position: 'absolute',
		backgroundColor: 'red',
		top: -70,
		height: 200,
		width: 200,
		left: 0,
		borderRadius: 70,
		transform: [ { rotate: '120deg' } ]
	},
	shape2: {
		position: 'absolute',
		backgroundColor: 'red',
		bottom: 5,
		height: 100,
		width: 100,
		right: 0,
		borderRadius: 40,
		zIndex: -20,
		transform: [ { rotate: '140deg' } ]
	},
	banner: {
		flex: 1,
		justifyContent: 'space-evenly',
		paddingHorizontal: 20
	},
	title: {
		fontSize: 64,
		fontWeight: 'bold',
		color: 'white',
		textShadowColor: 'rgba(0, 0, 0, 0.75)',
		textShadowOffset: { width: -1, height: 1 },
		textShadowRadius: 10
	},
	search: {
		backgroundColor: 'transparent',
		borderColor: 'transparent',
		color: '#0288D1'
	},
	searchBox: {},
	searchButton: {
		position: 'absolute',
		right: 0,
		bottom: 0
	},
	searchClear: {
		position: 'absolute',
		right: 30,
		bottom: 0
	}
})

export default Header
