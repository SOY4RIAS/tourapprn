import React, { Component } from 'reactn'
import { View } from 'react-native'
import { Card, Title, Paragraph } from 'react-native-paper'

class CardSite extends Component {
  render() {
    const { name, images, description } = this.props.data
    let image = 'https://firebasestorage.googleapis.com/v0/b/tourapp-330ac.appspot.com/o/default-image.jpg?alt=media&token=f5334572-e523-4894-be9d-3c97f05cbb46'
    if (images) {
      image = Object.values(images)[0]
    }
    return (
      <View style={{ padding: 5 }}>
        <Card elevation={2}>
          <Card.Cover source={{ uri: image }} />
          <Card.Content>
            <Title>{name}</Title>
            <Paragraph numberOfLines={3}>{description}</Paragraph>
          </Card.Content>
        </Card>
      </View>
    )
  }
}

export default CardSite
