import React, { Component } from 'reactn'
import { StyleSheet } from 'react-native'
import { Dialog, TextInput, Button } from 'react-native-paper'
import SiteModel from '../tools/models/SiteModel'

class ModalSite extends Component {
  constructor(props) {
    super(props)
    if (props['form'] !== undefined) {
      this.state = { form: { ...props.form } }
    } else {
      this.state = { form: { ...new SiteModel() } }
    }
  }

  _showDialog = () => this.setGlobal({ modalVisible: true })

  _hideDialog = () => {
    this.setState({ form: {} })
    this.setGlobal({ modalVisible: false })
  }

  render() {
    const { modalVisible } = this.global,
      { title, textBtn, action } = this.props,
      { input } = styles,
      { name, description } = this.state.form
    return (
      <Dialog visible={modalVisible} onDismiss={this._hideDialog}>
        <Dialog.Title>{title}</Dialog.Title>
        <Dialog.Content>
          <TextInput
            mode='outlined'
            style={input}
            label='Nombre'
            value={name}
            onChangeText={text =>
              this.setState(prev => ({ form: { ...prev.form, name: text } }))
            }
          />
          <TextInput
            style={input}
            mode='outlined'
            label='Descripcion'
            multiline={true}
            numberOfLines={4}
            value={description}
            onChangeText={text =>
              this.setState(prev => ({
                form: { ...prev.form, description: text }
              }))
            }
          />
          <Button>Foto</Button>
        </Dialog.Content>
        <Dialog.Actions>
          <Button onPress={() => action(this.state.form)}>
            {textBtn ? textBtn : 'OK'}
          </Button>
        </Dialog.Actions>
      </Dialog>
    )
  }
}

const styles = StyleSheet.create({
  input: {
    marginBottom: 5
  }
})

export default ModalSite
