import React, { Component } from 'reactn'
import './tools/GlobalState'
import Navigation from './navigation'
import firebase from 'react-native-firebase'
import { setItem, getItem } from './tools/storage'
import DeviceInfo from 'react-native-device-info'

import { listenNetworkStatus } from './tools/network'
import { checkPermission } from './tools/notifications'

export default class App extends Component {
	networkListenser = null
	notifyListener = null

	constructor() {
		super()
		this.loadSession()
	}

	loadSession = async () => {
		const user = await getItem('currentUser')
		if (user === null || user.additionalUserInfo.isNewUser) {
			try {
				firebase.auth().signInAnonymously().then(async credential => {
					await setItem('currentUser', credential)
					this.setGlobal({ currentUser: credential })
				})
			} catch (e) {}
		}
		else {
			this.setGlobal({ currentUser: user })
		}
		this._listenEvents()
		this._initNotificationsService()
	}

	_initNotificationsService = async () => {
		checkPermission()

		firebase.messaging().requestPermission().then(res => {
			firebase.messaging().getToken().then(token => {
				firebase.database().ref('/devices_token').child(DeviceInfo.getUniqueID()).set(token)
				setItem('notify', { register: true })
			})

			firebase.messaging().onTokenRefresh(token => {
				firebase.database().ref('/devices_token').child(DeviceInfo.getUniqueID()).set(token)
			})
		})
	}

	_notificationListener = () =>
		firebase.notifications().onNotification(notification => {
			const { notifications: { Android: { Priority: { Max } } } } = firebase
			notification.android.setChannelId('101')
			notification.android.setPriority(Max)
			notification.setData(notification.data)
			firebase.notifications().displayNotification(notification)
		})

	createChannel = () => {
		const channel = new firebase.notifications.Android.Channel(
			'101',
			'Notifications tour app',
			firebase.notifications.Android.Importance.Max
		).setDescription('Notification tray')
		firebase.notifications().android.createChannel(channel)
	}

	_listenEvents = () => {
		this.networkListenser = listenNetworkStatus()
	}

	componentWillUnmount() {
		if (this.networkListenser) {
			this.networkListenser()
		}

		if (this.notifyListener) {
			this.notifyListener()
		}
	}

	componentDidMount() {
		this.createChannel()
		this.notifyListener = this._notificationListener()
	}

	render() {
		return <Navigation />
	}
}
