export default function SiteModel() {
  this.name = String()
  this.description = String()
  this.image = String()
  this.coordinates = {
    latitude: Number(),
    longitude: Number()
  }
  this.email = String()
  this.address = String()
  this.page = String()
}
