import { DIRECTIONS_KEY } from '../constants'
import Polyline from '@mapbox/polyline'

export function getDistance(p1, p2) {
  rad = x => (x * Math.PI) / 180
  let R = 6378137 //radio de la tierra en metros
  let dLat = rad(p2.latitude - p1.latitude)
  let dLong = rad(p2.longitude - p1.longitude)
  let a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(rad(p1.latitude)) *
      Math.cos(rad(p2.latitude)) *
      Math.sin(dLong / 2) *
      Math.sin(dLong / 2)
  let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
  let d = R * c
  return d
}

export async function getDirections(from, to, mode = 'transit') {
  const origin = `${from.lat},${from.lan}`,
    destination = `${to.lat},${to.lan}`,
    URL = `https://maps.googleapis.com/maps/api/directions/json?origin=${origin}&destination=${destination}
&key=${DIRECTIONS_KEY}&mode=${mode}`

  try {
    const resBody = await fetch(URL)

    const respJSON = await resBody.json()

    let points = Polyline.decode(respJSON.routes[0].overview_polyline.points)
    let coords = points.map((point, index) => ({
      latitude: point[0],
      longitude: point[1]
    }))

    return coords
  } catch {
    return []
  }
}
