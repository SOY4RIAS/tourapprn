import { setGlobal, addReducer } from 'reactn'

setGlobal({
  currentUser: {},
  backupSites: [],
  sites: [],
  online: false,
  dialogCoor: false
})

//Reducers

addReducer('SEARCH_SITES', (global, dispatch, search) => {
  if (search == '') {
    return { sites: global.backupSites }
  }
  let sites = global.backupSites.filter(
    ({ name, description }) => name.toLowerCase().search(search.toLowerCase()) > -1
  )
  return {
    sites
  }
})
