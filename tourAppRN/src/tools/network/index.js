import { getGlobal, setGlobal } from 'reactn'
import NetInfo from '@react-native-community/netinfo'

export const evaluateNetwork = async () => {
  try {
    let a = await fetch(
      'https://us-central1-tourapp-330ac.cloudfunctions.net/checkConnection'
    )
    return true
  } catch (err) {
    console.log(err)
    return false
  }
}

export const listenNetworkStatus = () => {
  return NetInfo.addEventListener(async state => {
    const { ...currentGlobalState } = getGlobal()

    if (state.isConnected) {
      currentGlobalState.online = await evaluateNetwork()
      setGlobal(currentGlobalState)
      return
    }
    currentGlobalState.online = state.isConnected
    setGlobal(currentGlobalState)
  })
}
