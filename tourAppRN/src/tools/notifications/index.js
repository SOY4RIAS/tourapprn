import firebase from 'react-native-firebase';
import { getItem, setItem } from '../storage';

const getToken = async () => {
	let fcmToken = await getItem('fcmToken');
	if (!fcmToken) {
		fcmToken = await firebase.messaging().getToken();
		if (fcmToken) {
			await setItem('fcmToken', { fcmToken });
		}
	}
};

const requestPermission = async () =>
	firebase
		.messaging()
		.requestPermission()
		.then(() => {
			getToken();
		})
		.catch(error => {
			console.warn(`${error} permission rejected`);
		});

export const checkPermission = async () => {
	const enabled = await firebase.messaging().hasPermission();
	if (enabled) {
		getToken();
	}
	else {
		requestPermission();
	}
};
