import AS from '@react-native-community/async-storage'

export const setItem = async (key, value = {}) => {
  try {
    value = JSON.stringify(value)
    await AS.setItem(key, value)
    return true
  } catch (error) {
    return false
  }
}

export const getItem = async key => {
  try {
    let value = await AS.getItem(key)
    return JSON.parse(value)
  } catch (error) {
    return false
  }
}

export const removeItem = async key => {
  try {
    await AS.removeItem(key)
    return true
  } catch {
    return false
  }
}
