const functions = require('firebase-functions')
const admin = require('firebase-admin')

admin.initializeApp(functions.config().firebase)

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
exports.checkConnection = functions.https.onRequest((request, response) => {
	response.json({ status: true })
})

exports.sendNotification = functions.database.ref('/sites/{siteId}').onCreate(async (change, context) => {
	const Site = change.val()

	let snapshot = await admin.database().ref('/devices_token').once('value')

	let tokens = []

	let dbData = snapshot.toJSON()

	for ([ k, v ] of Object.entries(dbData)) {
		tokens.push(v)
	}

	try {
		let resp = await admin.messaging().sendToDevice(tokens, {
			notification: {
				title: 'Se ha Creado un nuevo Sitio',
				body: Site.name,
				sound: 'default'
			}
		})

		console.log(resp)
	} catch (e) {
		console.error('PANA SE ROMPIÓ', e)
	}
})

exports.evaluateComments = functions.database.ref('/sites/{siteId}/comments').onWrite((snapshot, context) => {
	const data = snapshot.after.toJSON()
	let total = 0
	let qty = 0

	for ([ k, v ] of Object.entries(data)) {
		qty++
		total += Number(v.stars)
	}

	console.log(total, qty)

	const qualification = total / qty

	return snapshot.before.ref.parent.update({ qualification })
})
